import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { handleError, ErrorHandler } from './helpers/error';

import logger from './logger';
import routes from './routes/v0';

const app = express();

/// //////////////////// app level middlewares //////////////////////////
app.use(helmet());
app.use(cors());

app.use((req, res, next) => {
  const ip = req.header['x-forwarded-for'] || req.connection.remoteAddress;
  logger.info(`${ip} ${req.hostname}: ${req.method} ${req.path}`, { method: '--- New Request ---' });
  next();
});

// app.use(protect);

app.use(express.json({ type: 'application/json', limit: '5mb' }));

/// //////////////////// backend routes ////////////////////////
app.use('/v0', routes);

// catch all request that are not handled
app.all('*', (req, res, next) => {
  const err = new ErrorHandler(404, `cant find ${req.originalUrl} on this server`);
  next(err, res);
});

// Error Handling - MUST BE LAST !
app.use((err, req, res, next) => { // eslint-disable-line
  handleError(err, res);
});

export default app;
