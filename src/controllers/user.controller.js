const upsertUserImpl = async (req, res, next) => {
  try {
    res.status(200).send();
  } catch (err) {
    next(err);
  }
};

const createUser = upsertUserImpl;
const updateUser = upsertUserImpl;

const deleteUser = async (req, res, next) => {
  try {
    res.status(200).send();
  } catch (err) {
    next(err);
  }
};

const getAllUsers = async (req, res, next) => {
  try {
    res.status(200).send();
  } catch (err) {
    next(err);
  }
};

const getUser = async (req, res, next) => {
  try {
    res.status(200).send();
  } catch (err) {
    next(err);
  }
};

export default {
  getUser,
  deleteUser,
  createUser,
  updateUser,
  getAllUsers,
};
