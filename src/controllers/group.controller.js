import { keyclockGroupToIamGroup } from '../helpers/group';
import KeycloakAdapter from '../services/KeycloakAdapter/keycloakServer';

const createGroup = async (req, res, next) => {
  try {
    const kc = new KeycloakAdapter(req.headers.authorization);
    await kc.Groups.create(req.body);
    res.status(201).send();
  } catch (err) {
    next(err);
  }
};

const createSubGroup = async (req, res, next) => {
  try {
    const { groupid: parentGroup } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    await kc.Groups.createSubGroup(parentGroup, req.body);
    res.status(201).send();
  } catch (err) {
    next(err);
  }
};

const getAllGroups = async (req, res, next) => {
  try {
    const kc = new KeycloakAdapter(req.headers.authorization);
    const groups = await kc.Groups.getAll();
    res.send(groups.map(keyclockGroupToIamGroup));
  } catch (err) {
    next(err);
  }
};

const updateGroupById = async (req, res, next) => {
  try {
    const { groupid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    await kc.Groups.updateById(groupid, req.body);
    res.send();
  } catch (err) {
    next(err);
  }
};

const deleteGroupById = async (req, res, next) => {
  try {
    const { groupid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    await kc.Groups.deleteById(groupid);
    res.send();
  } catch (err) {
    next(err);
  }
};

const getGroupById = async (req, res, next) => {
  try {
    const { groupid } = req.params;
    const kc = new KeycloakAdapter(req.headers.authorization);
    const group = await kc.Groups.getById(groupid);
    res.send(keyclockGroupToIamGroup(group));
  } catch (err) {
    next(err);
  }
};

export default {
  createGroup,
  createSubGroup,
  getAllGroups,
  getGroupById,
  updateGroupById,
  deleteGroupById,
};
