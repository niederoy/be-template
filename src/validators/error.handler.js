import { ErrorHandler } from '../helpers/error';

const handleValidationError = async (err, next) => {
  // handle Joi assertion errors
  const { details } = err;
  const message = details.map((errDetail) => errDetail.message).join(',');

  next(new ErrorHandler(400, message));
};

export default handleValidationError;
