import handleValidationError from './error.handler';

const validateSchema = async (schema, property, req, next) => {
  try {
    const joiOptions = { errors: { wrap: { label: '' } } };
    const data = await schema.validateAsync(req[property], joiOptions);

    req[property] = data;

    next();
  } catch (err) {
    handleValidationError(err, next);
  }
};

const errMessages = {
  alphaOnly: 'must contain only alphabet letters',
  alphaNum: 'must contain only alphabet letters or numbers',
  firstName: 'first name can contain only letters/dahses/spaces',
  lastName: 'last name can contain only letters/dahses/spaces',
};

const regex = {
  alphaOnly: /^[a-zA-Z]+$/,
  alphaNum: /^[a-zA-Z0-9]+$/,
  firstName: /^[A-Za-z -]+$/, // spaces and dashes allowed
  lastName: /^[A-Za-z -]+$/, // spaces and dashes allowed
};

export default {
  errMessages,
  regex,
  validateSchema,
};
