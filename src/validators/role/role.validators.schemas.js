import Joi from 'joi';
import common from '../common';

// role
const roleid = Joi.string().trim(); // TODO: enforce id rules
const name = Joi.string().trim().regex(common.regex.alphaNum).message(`name ${common.errMessages.alphaNum}`);

// authorization
const authorization = Joi.string(); // TODO: enforce authorization token rules

const postRole = Joi.object({
  name: name.required(),
});

const putRole = Joi.object({
  name: name.required(),
});

const idParam = Joi.object({
  roleid: roleid.required(),
});

const authHeader = Joi.object({
  authorization: authorization.required(),
});

export default {
  postRole,
  putRole,
  idParam,
  authHeader,
};
