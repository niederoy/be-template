import Joi from 'joi';
import common from '../common';

const userid = Joi.string().trim();
const email = Joi.string().email().lowercase({ force: true });
const firstName = Joi
  .string()
  .trim()
  .regex(common.regex.firstName)
  .message(`firstName ${common.errMessages.firstName}`)
  .lowercase({ force: true });
const lastName = Joi
  .string()
  .trim()
  .regex(common.regex.lastName)
  .message(`lastName ${common.errMessages.lastName}`)
  .lowercase({ force: true });
const authorization = Joi.string();

const userRequestRepresentation = Joi.object({
  firstName,
  lastName,
  email: email.required(),
});

const idParam = Joi.object({
  userid: userid.required(),
});

const authHeader = Joi.object({
  authorization: authorization.required(),
});

const putUser = userRequestRepresentation;
const postUser = userRequestRepresentation;

export default {
  postUser,
  putUser,
  idParam,
  authHeader,
};
