import qs from 'querystring';
import axios from 'axios';

const realmName = process.env.KEYCLOAK_REALM;
const baseUrl = process.env.KEYCLOAK_BASE_URL;

// eslint-disable-next-line import/prefer-default-export
export async function getAuthToken() {
  const jsonData = {
    client_id: 'admin-cli',
    grant_type: 'password',
    username: 'test',
    password: 'test',
    offlineToken: true,
  };
  const formData = qs.stringify(jsonData);

  const url = `${baseUrl}/realms/${realmName}/protocol/openid-connect/token`;
  const response = await axios({
    method: 'post',
    url,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data: formData,
  });

  return response.data.access_token;
}
