/* eslint-disable no-undef */
import express from 'express';
import request from 'supertest';
import bodyParser from 'body-parser';

import routes from '../../routes/v0';
import { getAuthToken } from '../utils';
import { handleError } from '../../helpers/error';

const app = express();
app.use(bodyParser.json());
app.use('/v0', routes);
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  handleError(err, res);
});
let bearerString;

describe('Test user endpoints', () => {
  beforeAll(async () => {
    try {
      const token = await getAuthToken();
      bearerString = `Bearer ${token}`;
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error('Couldnt Get Authorization header', err);
    }
  });
  afterAll(async () => {

  });

  const testUser = {
    firstName: 'brian',
    lastName: 'kernighan',
    email: 'brian@att.com',
  };

  const getTestUserId = async () => {
    const { body } = await request(app).get('/v0/users')
      .set('Authorization', bearerString)
      .send();
    const users = body;
    for (let idx = 0; idx < users.length; idx += 1) {
      if (users[idx].email === testUser.email) {
        return users[idx].id;
      }
    }

    return null;
  };

  test('POST user - empty body', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .send();
    expect(res.status).toBe(400);
  });

  test('POST user - only firstName', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .send({ firstName: testUser.firstName });
    expect(res.status).toBe(400);
  });

  test('POST user - only lastName', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .send({ lastName: testUser.lastName });
    expect(res.status).toBe(400);
  });

  test('POST user - only email', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .send({ email: testUser.email });
    expect(res.status).toBe(400);
  });

  test('POST user - without authorization token', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .send(testUser);
    expect(res.status).toBe(401);
  });

  test('POST user - valid', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .set('authorization', bearerString)
      .send(testUser);
    expect(res.status).toBe(201);
  });

  test('POST user - user already exists', async () => {
    const res = await request(app).post('/v0/users')
      .set('Content-Type', 'application/json')
      .set('authorization', bearerString)
      .send(testUser);
    expect(res.status).toBe(409);
  });

  // TODO: the test below are coupled to the test user from above !!! we need to address that !!
  test('GET ALL users - without authorization token', async () => {
    const res = await request(app).get('/v0/users');
    expect(res.status).toBe(401);
  });

  test('GET ALL users - valid', async () => {
    const res = await request(app).get('/v0/users')
      .set('Authorization', bearerString)
      .send();
    expect(res.status).toEqual(200);
  });

  test('GET user - without token', async () => {
    const testUserId = await getTestUserId();
    expect(testUserId).not.toEqual(null);

    const res = await request(app).get(`/v0/users/${testUserId}`)
      .set('Content-Type', 'application/json')
      .send();
    expect(res.status).toBe(401);
  });

  test('GET user - user id does not exist', async () => {
    const unexisted = '12366875-efdfdg-3435645';
    const res = await request(app).get(`/v0/users/${unexisted}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(404);
  });

  test('GET user - valid', async () => {
    const testUserId = await getTestUserId();
    expect(testUserId).not.toEqual(null);

    const res = await request(app).get(`/v0/users/${testUserId}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(200);
  });

  test('PUT user - without authorization token', async () => {
    const unexisted = '12366875-efdfdg-3435645';
    const res = await request(app).delete(`/v0/users/${unexisted}`)
      .send();
    expect(res.status).toBe(401);
  });

  test('PUT user - user id does not exist', async () => {
    const unexisted = '12366875-efdfdg-3435645';
    const res = await request(app).put(`/v0/users/${unexisted}`)
      .set('Authorization', bearerString)
      .send();
    expect(res.status).toBe(404);
  });

  test('PUT user - empty body', async () => {
    const testUserId = await getTestUserId();
    const res = await request(app).put(`/v0/users/${testUserId}`)
      .set('Authorization', bearerString)
      .send({});
    expect(res.status).toBe(200);

    const updateRes = await request(app).get(`/v0/users/${testUserId}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(200);

    expect(updateRes.body.firstName).toEqual(testUser.firstName);
    expect(updateRes.body.lastName).toEqual(testUser.lastName);
  });

  test('PUT user - update firstName', async () => {
    const testUserId = await getTestUserId();
    const newFirstName = 'johnny';
    const res = await request(app).put(`/v0/users/${testUserId}`)
      .set('Authorization', bearerString)
      .send({ firstName: newFirstName });
    expect(res.status).toBe(200);

    const updateRes = await request(app).get(`/v0/users/${testUserId}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(200);

    expect(updateRes.body.firstName).toEqual(newFirstName);
  });

  test('PUT user - update lastName', async () => {
    const testUserId = await getTestUserId();
    const newLastName = 'bach';
    const res = await request(app).put(`/v0/users/${testUserId}`)
      .set('Authorization', bearerString)
      .send({ lastName: newLastName });
    expect(res.status).toBe(200);

    const updateRes = await request(app).get(`/v0/users/${testUserId}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(200);

    expect(updateRes.body.lastName).toEqual(newLastName);
  });

  test('GET user groups - without authorization token', async () => {
    const testUserId = await getTestUserId();
    expect(testUserId).not.toEqual(null);

    const res = await request(app).get(`/v0/users/${testUserId}/groups`)
      .send();
    expect(res.status).toBe(401);
  });

  test('GET user groups - user id does not exist', async () => {
    const unexisted = '231234-rwef-2132-dsda';

    const res = await request(app).get(`/v0/users/${unexisted}/groups`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(404);
  });

  test('GET user groups - valid', async () => {
    const testUserId = await getTestUserId();
    expect(testUserId).not.toEqual(null);

    const res = await request(app).get(`/v0/users/${testUserId}/groups`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(200);
  });

  test('DELETE user - without authorization token', async () => {
    const unexisted = '12366875-efdfdg-3435645';
    const res = await request(app).delete(`/v0/users/${unexisted}`)
      .send();
    expect(res.status).toBe(401);
  });

  test('DELETE user - user id does not exist', async () => {
    const unexisted = '12366875-efdfdg-3435645';
    const res = await request(app).delete(`/v0/users/${unexisted}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(404);
  });

  test('DELETE user - valid', async () => {
    const testUserId = await getTestUserId();
    expect(testUserId).not.toEqual(null);

    const res = await request(app).delete(`/v0/users/${testUserId}`)
      .set('authorization', bearerString)
      .send();
    expect(res.status).toBe(200);
  });
});
