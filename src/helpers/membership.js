const generateMembershipRoleName = (groupId, roleName) => `${groupId}::${roleName}`;
const extractRoleFromMembershipRole = (GroupRoleName) => GroupRoleName.split('::', 2)[1];
const extractGroupIdFromMembershipRole = (GroupRoleName) => GroupRoleName.split('::', 2)[0];
const isMembershipRoleInGroup = (groupId, roleName) => roleName
  .split('::', 1)[0] === groupId;

export {
  generateMembershipRoleName,
  extractRoleFromMembershipRole,
  isMembershipRoleInGroup,
  extractGroupIdFromMembershipRole,
};
