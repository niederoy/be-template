import logger from '../logger';

class ErrorHandler extends Error {
  constructor(statusCode, message) {
    super();
    if (statusCode && message) {
      this.status = statusCode;
      this.message = message;
    }
  }
}

const handleAxiosError = (err) => {
  if (err.response) {
    return {
      status: err.response.status,
      message: err.response.data.errorMessage,
    };
  }
  return undefined;
};

const handleError = (err, res) => {
  let { status, message } = err;

  const axiosError = handleAxiosError(err);
  if (axiosError) {
    status = axiosError.status;
    message = axiosError.message;
  }
  if (!status) {
    status = 500;
    message = 'Internal Error';
  }

  logger.error(err.message, { method: 'handleError' });

  res.status(status).json({
    status,
    message,
  });
};

export {
  handleError,
  ErrorHandler,
};
