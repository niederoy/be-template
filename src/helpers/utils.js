export const createAuthorizationHeaders = (req) => {
  if (req.headers.authorization) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: req.headers.authorization,
    };

    return headers;
  }

  return {};
};

export default createAuthorizationHeaders;
