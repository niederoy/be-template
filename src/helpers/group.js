// eslint-disable-next-line import/prefer-default-export
export const keyclockGroupToIamGroup = ({
  id, name, path, subGroups,
}) => {
  let sanitizedSubGroups = [];
  if (subGroups) {
    sanitizedSubGroups = subGroups.map(keyclockGroupToIamGroup);
  }

  return ({
    id,
    name,
    path,
    subGroups: sanitizedSubGroups,
  });
};
