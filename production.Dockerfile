FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./

RUN apt update && apt install -y
RUN npm install --production

COPY .babelrc ./
COPY certs ./certs
COPY src ./src
RUN ["npm","run", "build"]

EXPOSE 4040 8080

CMD ["npm","run", "start"]
